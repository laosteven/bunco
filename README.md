# Dice Game: Bunco #
-------
Build a **dice game** which allows user to set:

* the amount of dice,
* the number of face for each dice,
* and the **number of players** they want. 

For now, the program is set to 3 dices with 6 faces and follows the **[Bunco](https://en.wikipedia.org/wiki/Bunco)** game rules.

![121.png](https://bitbucket.org/repo/RkagGj/images/117737808-121.png)

After 6 turns, the player with the highest score wins.

# Objectives #
-------
* Use `JUnit` to test the scoring system.
* Implement MVC pattern.
* Familiarize with `Java Swing`.

# How do I get set up? #
-------
A runnable `.jar` file is available in the project's root folder.

# For more information #
-------
Visit the following website: [**Object Oriented Conception** (LOG121)](https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=log121) [*fr*]