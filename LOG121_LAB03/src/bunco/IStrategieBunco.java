/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		IStrategieBunco.java
 * Date cree: 			2013-07-01
 * Date dern. modif.: 	2013-07-01
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-07-01 Version initiale
 ***************************************************************/
package bunco;

import java.util.Arrays;

import cadriciel.IStrategie;
import cadriciel.Joueur;

/************************************************************
 * Classe qui g�re les applications des r�glements du bunco.
 ************************************************************/
public class IStrategieBunco implements IStrategie {

	/************************************************************
	 * Attributs
	 ************************************************************/
	private int chiffre;

	/************************************************************
	 * Garde en memoire le chiffre similaire pour comparaison
	 * 
	 * @param chiffreRef
	 *            Le chiffre de des identique
	 ************************************************************/
	public void setChiffreSimilaire(int chiffreRef) {
		this.chiffre = chiffreRef;
	}

	/************************************************************
	 * @return Le chiffre similaire obtenu
	 ************************************************************/
	public int getChiffreSimilaire() {
		return chiffre;
	}

	/************************************************************
	 * Calcule le score obtenu par un joueur
	 * 
	 * @param pointageDe
	 *            Le r�sultat du brassage des d�s
	 * @param tour
	 *            Le num�ro de tour actuel.
	 * @return Le pointage du joueur.
	 ************************************************************/
	public int calculerScoreTour(int[] pointageDe, int tour) {

		// Initialisation d'attribut
		int pointsTour = 0;
		int identique = 0;

		// On cherche si le tableau recu possede des valeurs identiques
		identique = tabIntIdentique(pointageDe);

		// Lorsqu'on obtient 3 chiffres identiques
		if (identique == 3) {

			// Lorsque les chiffres correspondent au nb de tours
			if (getChiffreSimilaire() == tour) {

				// Le joueur gagne 21 points
				pointsTour = 21;

			} else {

				// Sinon, 5 points gagnes
				pointsTour = 5;

			} // Fin du if/else

		} else if (identique == 1 && getChiffreSimilaire() == tour) {

			// 2 points gagnes
			pointsTour = 2;

		} else {

			// On verifie les chiffres individuellement
			for (int boucle = 0; boucle < pointageDe.length; boucle++) {

				// Pour chaque chiffre equivaut au nombre de tour
				if (pointageDe[boucle] == tour)
					// 1 point gagne
					pointsTour = 1;

			}

		} // Fin du if/else

		return pointsTour;

	}

	/************************************************************
	 * M�thode qui v�rifie si les valeurs d'un tableau int[] sont identiques
	 * 
	 * @param tabInt
	 *            Un tableau qui contient les chiffres de des
	 * @return Valeur de similarite entre valeurs
	 ************************************************************/
	private int tabIntIdentique(int[] tabInt) {

		int index1 = 0;
		int index2 = 0;
		int resultat = 0;

		// Le cas s'applique seulement lorsqu'on possede plus qu'un de.
		if (tabInt.length > 1) {

			// V�rifie si les valeurs ne sont pas identiques.
			while (index1 < tabInt.length) {

				// On s'assure de ne pas comparer la meme chose
				if (index1 != index2) {

					// Si les chiffres sont identique
					if (tabInt[index1] == tabInt[index2]) {

						// Incremente le resultat
						resultat++;

						// On garde en memoire le chiffre obtenu
						setChiffreSimilaire(tabInt[index1]);

					}

				}

				// Tant que l'index2 ne depasse pas les cases du tableau
				if (index2 < tabInt.length - 1) {

					// Incremente l'index2 pour la prochaine comparaison
					index2++;

				} else {

					// Incremente pour le prochain de
					index1++;
					index2 = 0;

				}

			} // Fin du while

		} // Fin du if

		return resultat / 2;

	}

	/************************************************************
	 * Methode qui permet de determiner si le joueur peut brasser une autre fois
	 * ou non.
	 * 
	 * @param points
	 *            Les points du dernier brassage
	 * @return boolean
	 ************************************************************/
	public boolean finBrasse(int points) {

		boolean finBrasse = false;

		// Lorsque le joueur obtient un bunco
		if (points == 21) {

			// Il ne peut plus brasser
			finBrasse = true;

		} else if (points == 0) {

			// Sinon si aucun de ses des sont identiques
			finBrasse = true;

		}

		return finBrasse;
	}

	/************************************************************
	 * Calcule le vainqueur du jeu
	 ************************************************************/
	public Joueur[] calculerLeVainqueur(Joueur[] tabJoueur) {
		
		// Creation de tableaux
		Joueur[] tabJoueurSort = calculerPosition(tabJoueur);
		Joueur[] tabJoueurGagnant;

		// Initialisation
		int boucle = 1;
		while (tabJoueurSort[0].getPointage() == tabJoueurSort[boucle].getPointage()) {
			boucle++;
		}
		tabJoueurGagnant = new Joueur[boucle];
		for (int j = 0; boucle > j; j++) {
			tabJoueurGagnant[j] = tabJoueurSort[j] ;
		}
		// Classifie les joueurs
		Arrays.sort(tabJoueur);

		// Retourne le gagnant
		return tabJoueurGagnant;

	}

	/************************************************************
	 * Calcule la position du joueur
	 ************************************************************/
	public Joueur[] calculerPosition(Joueur tabJoueur[]) {

		// Classifie les joueurs
		Arrays.sort(tabJoueur);

		// Retourne le tableau classifie
		return tabJoueur;

	}

}
