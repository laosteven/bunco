/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		AffichagePopup.java
 * Date cree: 			2013-06-27
 * Date dern. modif.: 	2013-06-30
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-06-27 Version initiale
 * 2013-06-30 Ajout de commentaires, correction PMD, ajout
 * 			  d'accesseurs/mutateurs, ajustement de FocusListener
 ***************************************************************/
package bunco;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cadriciel.Fabrique;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.*;

public class AffichagePopup implements ActionListener, ChangeListener,
		FocusListener {

	/************************************************************
	 * Attributs
	 ************************************************************/
	private JFrame frame;
	private JPanel contentPane;
	private JTabbedPane tabPrincipal = new JTabbedPane(JTabbedPane.TOP);
	private JPanel panneauParamBunco = new JPanel();
	private JButton btnJouer = new JButton("Jouer");
	private JButton btnQuitter = new JButton("Quitter");

	private JSpinner spinJoueur;
	private JSpinner spinNbDes;
	private JSpinner spinNbFace;
	
	private JPanel panneauPropos;
	private JPanel panneauImage;
	private ImageIcon image;
	private JLabel lblLogo;

	private int nbJoueur;
	private int nbDes;
	private int nbFaces;

	/************************************************************
	 * Constructeur par defaut
	 ************************************************************/
	public AffichagePopup() {

		// Creation de la fenetre
		initComposant();

	}

	/************************************************************
	 * Configuration de la fenetre AffichagePopup
	 ************************************************************/
	public void initComposant() {

		// Creation de la fenetre principale
		frame = new JFrame();

		// Configuration principale du JFrame
		frame.setTitle("Laboratoire 3 | Cadriciel pour jeu de des");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 540, 300);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		// Creation du panneau
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		tabPrincipal.setBounds(0, 0, 536, 272);
		contentPane.add(tabPrincipal);

		// Ajout d'un onglet: Bunco+
		tabPrincipal.addTab("Bunco+", null, panneauParamBunco, null);
		panneauParamBunco.addFocusListener(this);
		panneauParamBunco.setLayout(null);

		// Ajout du bouton 'Jouer'
		btnJouer.setBounds(421, 200, 89, 23);
		panneauParamBunco.add(btnJouer);
		btnJouer.addActionListener(this);

		// Ajout du bouton 'Quitter'
		btnQuitter.setBounds(322, 200, 89, 23);
		panneauParamBunco.add(btnQuitter);
		btnQuitter.addActionListener(this);

		// Message de bienvenu
		JLabel lblIntroduction = new JLabel(
				"<html>"
						+ "Bienvenue dans le logiciel de creation de jeu de de!<br>"
						+ "Choisissez vos parametres pour la partie de <b>Bunco+</b>.<br><br>"
						+ "Les regles du jeu se retrouvent dans le lien suivant: "
						+ "<a href=\"http://www.worldbunco.com/rules.html\">"
						+ "World Bunco"
						+ "</a><br><br>"
						+ "Le jeu est automatiquement ajuste avec trois des a six faces."
						+ "</html>");
		lblIntroduction.setBounds(10, 11, 500, 92);
		panneauParamBunco.add(lblIntroduction);

		// Ajout d'un separateur
		JSeparator sepHorizontal = new JSeparator();
		sepHorizontal.setBounds(10, 114, 500, 2);
		panneauParamBunco.add(sepHorizontal);

		// Titre du jeu
		JLabel lblTitreBunco = new JLabel("Bunco+");
		lblTitreBunco.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTitreBunco.setBounds(10, 127, 99, 23);
		panneauParamBunco.add(lblTitreBunco);

		// Spinner pour le regagle du nombre de joueur
		spinJoueur = new JSpinner();
		spinJoueur.addChangeListener(this);
		spinJoueur.setBounds(112, 161, 49, 23);
		spinJoueur.setModel(new SpinnerNumberModel(new Integer(2), new Integer(
				2), null, new Integer(1)));

		// Titre du spinJoueur
		JLabel lblTitreJoueur = new JLabel("Nombre de joueurs:");
		lblTitreJoueur.setBounds(10, 161, 99, 23);
		panneauParamBunco.add(lblTitreJoueur);

		// Spinner pour le reglage du nombre de des
		spinNbDes = new JSpinner();
		spinNbDes.addChangeListener(this);
		spinNbDes.setBounds(280, 161, 49, 23);
		spinNbDes.setModel(new SpinnerNumberModel(new Integer(3), null, null,
				new Integer(1)));
		spinNbDes.setEnabled(false);
		
		// Titre du spinNbDes
		JLabel lblNbDes = new JLabel("Nombre de des:");
		lblNbDes.setBounds(186, 161, 89, 23);
		panneauParamBunco.add(lblNbDes);
		
		// Spinner pour le reglage du nombre de faces de des
		spinNbFace = new JSpinner();
		spinNbFace.addChangeListener(this);
		spinNbFace.setBounds(449, 161, 49, 23);
		spinNbFace.setModel(new SpinnerNumberModel(new Integer(6), null, null,
				new Integer(1)));
		spinNbFace.setEnabled(false);
		
		// Titre du spinFace
		JLabel lblNbFaces = new JLabel("Nombre de faces:");
		lblNbFaces.setBounds(350, 161, 89, 23);
		panneauParamBunco.add(lblNbFaces);
		
		// Ajout des spinners
		panneauParamBunco.add(spinJoueur);
		panneauParamBunco.add(spinNbDes);
		panneauParamBunco.add(spinNbFace);

		// Onglet 'A propos'
		panneauPropos = new JPanel();
		tabPrincipal.addTab("A propos", null, panneauPropos, null);
		panneauPropos.setLayout(null);

		// Description de l'onglet 'A propos'
		JLabel lblPropos = new JLabel(
				"<html>" 
				+ "Cours:   LOG121 <br>"
				+ "Session: E2013 <br>" 
				+ "Groupe:  01 <br>"
				+ "Projet: Laboratoire #3<br>" 
				+ "Etudiant(e)s:" 
				+ "<ol>"
				+ "<li>Anass Maii</li>" 
				+ "<li>Steven Lao</li>"
				+ "<li>Frederic Marquis</li>" 
				+ "</ol>"
				+ "Professeur : Patrice Boucher" 
				+ "</html>");

		// Positionnement du message 'A propos'
		lblPropos.setHorizontalAlignment(SwingConstants.CENTER);
		lblPropos.setBounds(10, 11, 222, 207);
		panneauPropos.add(lblPropos);

		// Ajout d'un separateur vertical
		JSeparator sepVertical = new JSeparator();
		sepVertical.setOrientation(SwingConstants.VERTICAL);
		sepVertical.setBounds(255, 11, 1, 207);
		panneauPropos.add(sepVertical);
		
		// Presentation du logo de l'ETS
		panneauImage = new JPanel(new BorderLayout());
		panneauImage.setBounds(266, 11, 242, 212);
		panneauPropos.add(panneauImage);
		
		// Lien de l'image
		image = new ImageIcon("image/ETS_icon.jpg");
		lblLogo = new JLabel("", image, JLabel.CENTER);
		panneauImage.add(lblLogo, BorderLayout.CENTER);
		
		// Validation de la fenetre
		frame.validate();
		frame.repaint();

	}

	/************************************************************
	 * Ecouteurs JButtons
	 ************************************************************/
	@Override
	public void actionPerformed(ActionEvent action) {

		// Quitter l'application
		if (action.getSource() == btnQuitter)
			System.exit(0);

		// Lorsque le joueur clique sur 'Jouer'
		if (action.getSource() == btnJouer) {

			// Prise en references des valeurs mises par l'utilisateur
			nbJoueur 	= (int) spinJoueur.getValue();
			nbDes 		= (int) spinNbDes.getValue();
			nbFaces 	= (int) spinNbFace.getValue();

			// Les parametres sont envoyes dans la classe Fabrique
			new Fabrique(nbJoueur, nbDes, nbFaces);

			// On ferme cette fenetre pour reduire l'espace memoire
			frame.dispose();

		}

	}

	/************************************************************
	 * Ecouteurs JSliders
	 ************************************************************/
	@Override
	public void stateChanged(ChangeEvent action) {

		// Les chiffres ne doivent pas etre a zero
		if ((int) spinJoueur.getValue() < 2 || 
				(int) spinNbDes.getValue() < 1 || 
				(int) spinNbFace.getValue() < 2)
			btnJouer.setEnabled(false);
		else
			btnJouer.setEnabled(true);

	}

	/************************************************************
	 * Ecouteurs JTabbedPane
	 * 
	 * Aucune methode est ecrite puisqu'on le framework joue seulement le jeu
	 * Bunco+
	 ************************************************************/
	@Override
	public void focusGained(FocusEvent event) {

		// Pour respecter la norme PMD, on ne laisse pas la methode vide
		frame.repaint();

	}

	@Override
	public void focusLost(FocusEvent event) {
		frame.repaint();
	}

}
