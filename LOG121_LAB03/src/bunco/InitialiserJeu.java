/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		InitialiserJeu.java
 * Date cree: 			2013-06-27
 * Date dern. modif.: 	2013-06-30
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-06-27 Version initiale
 * 2013-06-30 Ajout de commentaires, correction PMD, changement
 * 			  du Look and Feel
 ***************************************************************/
package bunco;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/***************************************************************
 * Classe qui permet d'initialiser le jeu avec une configuration
 * Look and Feel definit.
 ***************************************************************/
public class InitialiserJeu {

	public static void main(String[] args) {

		// Ajustement du Look and Feel
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		// Creation de la fenetre de parametre pour jeu
		new AffichagePopup();

	}

}
