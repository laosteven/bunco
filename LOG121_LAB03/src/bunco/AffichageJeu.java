/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		JeuAffiche.java
 * Date cree: 			2013-06-27
 * Date dern. modif.: 	2013-06-30
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-06-27 Version initiale
 ***************************************************************/
package bunco;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.TitledBorder;

/************************************************************
 * Classe qui genere l'affichage de la fenetre de jeu ainsi que l'affichage de
 * differents messages.
 * 
 * Toute information est genere a partir de la classe JeuBunco. Cette classe ne
 * fait qu'afficher les valeurs, aucune operation.
 ************************************************************/
public class AffichageJeu implements ActionListener {

	/************************************************************
	 * Constantes
	 ************************************************************/
	private final String MESSAGE_REC = "Voulez-vous vraiment recommencer?";

	private final String MESSAGE_QUITTER = "Voulez-vous vraiment quitter?";

	private final String TITRE_AVERTIR = "Bunco + | Attention!";

	private final String TITRE_GAGNANT = "Bunco + | Vainqueur";

	private final Color SELECT = new Color(245, 222, 179);

	/************************************************************
	 * Attributs
	 ************************************************************/
	private JeuBunco paramJeu;
	private int nbJoueur;

	private JFrame frame;

	private JPanel contentPane;
	private JPanel panneauJeu;
	private JPanel panneauPointage;

	private JLabel lblTitre;
	private JLabel lblDeAffiche1;
	private JLabel lblDeAffiche2;
	private JLabel lblDeAffiche3;
	private JLabel[] tabDes;
	private ImageIcon[] tabImageDes;

	private JLabel lblPosition;
	private JLabel lblMessage;
	private JLabel lblTours;
	private JLabel lblPointActuel;
	private JLabel lblPointSomme;

	private JSeparator sepVertical;
	private JButton btnAction;
	private JButton btnRetour;
	private JButton btnNouvellePartie;

	private JPanel[] tabPanneauDe;
	private JPanel[] tabPanneauSec;
	private JLabel[] tabJoueur;
	private JLabel[] tabPoints;

	private int reponse;
	private String vainqueur;

	/************************************************************
	 * Mutateurs
	 ************************************************************/

	/************************************************************
	 * Modifie les valeurs de des dans la table de jeu
	 * 
	 * @param quelDe
	 *            Le numero de des qu'on desire changer
	 * @param chiffre
	 *            Le chiffre qu'on veut afficher
	 ************************************************************/
	public void setDe(int quelDe, int chiffre) {
		// this.tabDes[quelDe].setText(String.valueOf(chiffre));
		this.tabDes[quelDe].setIcon(tabImageDes[chiffre - 1]);
	}

	/************************************************************
	 * Modifie le texte du point gagne pour un tour
	 * 
	 * @param pointObtenu
	 *            Le pointage a afficher
	 ************************************************************/
	public void setLblSomme(int pointObtenu) {
		this.lblPointSomme.setText(String.valueOf(pointObtenu));
	}

	/************************************************************
	 * Modifie le message pour l'utilisateur
	 * 
	 * @param messageRecu
	 *            Le message a afficher
	 ************************************************************/
	public void setLblMessage(String messageRecu) {
		this.lblMessage.setText(messageRecu);
	}

	/************************************************************
	 * Modifie le pointage accumule de l'utilisateur
	 * 
	 * @param pointActuel
	 ************************************************************/
	public void setLblPointage(int pointActuel) {
		this.lblPointActuel.setText(String.valueOf(pointActuel));
	}

	/************************************************************
	 * Modifie le nombre de tours
	 * 
	 * @param tourActuel
	 *            Le tour actuel a afficher
	 ************************************************************/
	public void setLblTours(int tourActuel) {
		this.lblTours.setText(String.valueOf(tourActuel) + " / 6 tours");
	}

	/************************************************************
	 * Modifie la position de pointage du joueur
	 * 
	 * @param positionRef
	 *            Position actuelle du joueur a afficher
	 ************************************************************/
	public void setLblPosition(int positionRef) {
		this.lblPosition.setText(String.valueOf(positionRef));
	}

	/************************************************************
	 * Modifie l'affichage du pointage de chaque joueur
	 * 
	 * @param tabPointsRef
	 *            Tableau de pointage de chaque joueur
	 ************************************************************/
	public void setTabPoints(int[] tabPointsRef) {

		for (int boucle = 0; boucle < paramJeu.listeJoueurs.getTabJoueur().length; boucle++) {
			this.tabPoints[boucle].setText(String.valueOf(tabPointsRef[boucle])
					+ " point(s)");
		}

	}

	/************************************************************
	 * Contructeur par defaut
	 * 
	 * Affichage de l'interface graphique
	 ************************************************************/
	public AffichageJeu(JeuBunco paramJeuRef) {

		// Prise de la classe de parametre en reference
		this.paramJeu = paramJeuRef;

		// Prise en reference du nombre de joueurs dans la partie
		nbJoueur = paramJeu.listeJoueurs.getTabJoueur().length;

		// Initiation des composantes
		initComposant();

	}

	/************************************************************
	 * Methode qui permet de creer la fenetre du jeu Bunco
	 ************************************************************/
	public void initComposant() {

		// Construction de la fenetre principale
		frame = new JFrame();
		frame.setTitle("Bunco + | Fenetre de jeu");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 600, 400);

		// Construction du panneau principal
		contentPane = new JPanel();
		frame.setContentPane(contentPane);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		contentPane.setLayout(null);

		// Separateur vertical
		sepVertical = new JSeparator();
		sepVertical.setOrientation(SwingConstants.VERTICAL);
		sepVertical.setBounds(300, 10, 1, 350);
		contentPane.add(sepVertical);

		/************************************************************
		 * JPanel
		 ************************************************************/

		// Panneau de jeu
		panneauJeu = new JPanel();
		panneauJeu.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Table de jeu",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panneauJeu.setBounds(10, 44, 280, 223);
		panneauJeu.setLayout(new GridLayout(1, 0, 0, 0));
		contentPane.add(panneauJeu);
		
		// Creation du panneau de des
		creationPanneauDe();

		// Panneau de pointage
		panneauPointage = new JPanel();
		panneauPointage.setBorder(new TitledBorder(null,
				"Pointage des joueurs", TitledBorder.LEADING, TitledBorder.TOP,
				null, null));
		panneauPointage.setBounds(311, 86, 273, 232);
		contentPane.add(panneauPointage);
		panneauPointage.setLayout(new GridLayout(0, 1, 0, 0));

		// Creation des panneaux secondaires
		creationPanneauJoueur();


		/************************************************************
		 * JLabel
		 ************************************************************/

		// Titre du jeu choisi
		lblTitre = new JLabel("Bunco+");
		lblTitre.setFont(new Font("Dialog", Font.BOLD, 20));
		lblTitre.setBounds(10, 10, 129, 28);
		contentPane.add(lblTitre);

		// Message du jeu
		lblMessage = new JLabel();
		lblMessage.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Message",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		lblMessage.setText("Bienvenu!");
		lblMessage.setHorizontalAlignment(SwingConstants.CENTER);
		lblMessage.setBounds(10, 278, 129, 40);
		contentPane.add(lblMessage);

		// Nombre de tours du jeu
		lblTours = new JLabel();
		lblTours.setBorder(new TitledBorder(null, "Tours",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		lblTours.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblTours.setText("1 / 6 tours");
		lblTours.setHorizontalAlignment(SwingConstants.CENTER);
		lblTours.setBounds(149, 278, 141, 40);
		contentPane.add(lblTours);

		// Position de l'utilisateur
		lblPosition = new JLabel();
		lblPosition.setBorder(new TitledBorder(null, "Position",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		lblPosition.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblPosition.setHorizontalAlignment(SwingConstants.CENTER);
		lblPosition.setText("-");
		lblPosition.setBounds(311, 11, 63, 65);
		contentPane.add(lblPosition);

		// Pointage de l'utilisateur
		lblPointActuel = new JLabel();
		lblPointActuel.setBorder(new TitledBorder(null, "Points",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		lblPointActuel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblPointActuel.setText("0");
		lblPointActuel.setHorizontalAlignment(SwingConstants.CENTER);
		lblPointActuel.setBounds(394, 10, 90, 66);
		contentPane.add(lblPointActuel);

		// Pointage gagne par l'utilisateur
		lblPointSomme = new JLabel();
		lblPointSomme.setBorder(new TitledBorder(null, "+ Somme",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		lblPointSomme.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblPointSomme.setHorizontalAlignment(SwingConstants.CENTER);
		lblPointSomme.setBounds(485, 11, 99, 65);
		contentPane.add(lblPointSomme);

		/************************************************************
		 * Affichage de des
		 ************************************************************/

		// Creation des des
		lblDeAffiche1 = new JLabel();
		lblDeAffiche2 = new JLabel();
		lblDeAffiche3 = new JLabel();

		// Tableau de des pour faciliter les modifications
		tabDes = new JLabel[] { lblDeAffiche1, lblDeAffiche2, lblDeAffiche3 };

		// Association d'image avec les JLabels
		// Pour afficher des chiffres, effacer la ligne suivante
		associeImageDe();

		// Configuration pour chaque de
		for (int boucle = 0; boucle < tabDes.length; boucle++) {

			// Ajustement centree
			tabDes[boucle].setHorizontalAlignment(SwingConstants.CENTER);

			// Modification de la taille de police
			// tabDes[i].setFont(new Font("Tahoma", Font.BOLD, 38));

			// Les des s'affichent tous a 1
			setDe(boucle, 1);

			// Ajout dans le panneau de jeu
			tabPanneauDe[boucle].add(tabDes[boucle]);

		}

		/************************************************************
		 * JButton
		 ************************************************************/

		// Bouton de brassage de des
		btnAction = new JButton("Brasser");
		btnAction.setBounds(6, 329, 284, 31);
		btnAction.addActionListener(this);
		contentPane.add(btnAction);

		// Nouvelle partie
		btnNouvellePartie = new JButton("Nouvelle partie");
		btnNouvellePartie.setBounds(311, 329, 129, 31);
		btnNouvellePartie.addActionListener(this);
		contentPane.add(btnNouvellePartie);

		// Bouton pour retourner aux parametres
		btnRetour = new JButton("Retour");
		btnRetour.setBounds(455, 329, 129, 31);
		btnRetour.addActionListener(this);
		contentPane.add(btnRetour);

		// Mise a jour de l'affichage
		rafraichir();

	}

	/************************************************************
	 * Chaque de est associe selon l'image quelle represente.
	 ************************************************************/
	public void associeImageDe() {

		// Creation du tableau
		tabImageDes = new ImageIcon[paramJeu.listeDe.getTabDe()[0].NBFACES];

		for (int boucle = 0; boucle < tabImageDes.length; boucle++) {

			// Recuperation de l'image
			tabImageDes[boucle] = new ImageIcon("image/" + (boucle + 1)
					+ ".png");

		}

	}
	
	/************************************************************
	 * Construction d'un panneau pour contenir un de
	 ************************************************************/
	public void creationPanneauDe(){
		
		// Initialisation
		tabPanneauDe = new JPanel[paramJeu.listeDe.getTabDe().length];

		for(int boucle = 0; boucle < tabPanneauDe.length; boucle++){
			
			// Creation du panneau de des
			tabPanneauDe[boucle] = new JPanel();
			tabPanneauDe[boucle].setLayout(new GridLayout(0, 1, 0, 0));
			panneauJeu.add(tabPanneauDe[boucle]);

		}
		
	}

	/************************************************************
	 * Creation des panneaux secondaires pour les autres joueurs
	 * 
	 * @param panneauPrincip
	 ************************************************************/
	public void creationPanneauJoueur() {

		// Creation de tableaux de donnees pour faciliter les modifs
		tabPanneauSec = new JPanel[nbJoueur];
		tabJoueur = new JLabel[nbJoueur];
		tabPoints = new JLabel[nbJoueur];

		// Creation d'une boucle pour chaque joueur
		for (int boucle = 0; boucle < nbJoueur; boucle++) {

			// Creation du panneau secondaire
			tabPanneauSec[boucle] = new JPanel();
			tabPanneauSec[boucle].setLayout(new GridLayout(0, 2, 0, 0));

			// Creation du titre du joueur
			tabJoueur[boucle] = new JLabel("Joueur"
					+ String.valueOf(boucle + 1));
			tabJoueur[boucle].setHorizontalAlignment(SwingConstants.LEFT);

			// Creation du titre des pointages
			tabPoints[boucle] = new JLabel("0 point(s)");
			tabPoints[boucle].setHorizontalAlignment(SwingConstants.LEFT);

			// Ajout des titres dans le panneau secondaire
			tabPanneauSec[boucle].add(tabJoueur[boucle]);
			tabPanneauSec[boucle].add(tabPoints[boucle]);

			// Ajout du panneau secondaire dans le panneau principal
			panneauPointage.add(tabPanneauSec[boucle]);

		} // Fin du for

	}

	/************************************************************
	 * Validation et mise-a-jour pour afficher la page complete
	 ************************************************************/
	public void rafraichir() {

		// Validation et mise a jour de la fenetre
		frame.validate();
		frame.repaint();

		// Si le jeu n'est pas termine
		if (!paramJeu.isTerminer())
			// Mise a jour du nombre de tours
			setLblTours(paramJeu.getTourActuel());

		// Position du premier joueur
		setLblPosition(paramJeu.getPosition());

		// On affiche le pointage pour chaque joueur
		setTabPoints(paramJeu.getTabPointage());

		// Mise en evidence du joueur actif
		tourJoueurEvidence();

		// Mise en evidence du de qui correspond au #tour
		deSimilaireEvidence();

	}

	/************************************************************
	 * Mise en evidence du joueur au moment de jouer dans le tableau de pointage
	 ************************************************************/
	public void tourJoueurEvidence() {

		// Pour un nombre specifique de joueur
		for (int boucle = 0; boucle < nbJoueur; boucle++) {

			// On compare quel joueur est en train de jouer actuellement
			if (paramJeu.getJoueurActuel().NOM.toString().equals(
					tabJoueur[boucle].getText())) {

				// Ce joueur sera surligne en orange
				tabPanneauSec[boucle].setBackground(SELECT);
			} else {

				// Le reste possede aucune couleur
				tabPanneauSec[boucle].setBackground(null);

			} // Fin du if/else

		} // Fin du for

	}

	/************************************************************
	 * Mise en evidence du des identique au numero de tour
	 ************************************************************/
	public void deSimilaireEvidence() {

		// On cherche parmi les des du panneau
		for (int nbDes = 0; nbDes < tabDes.length; nbDes++) {

			// Lorsque le chiffre correspond au numero de tour
			if (paramJeu.listeDe.getTabDe()[nbDes].getChiffre() == paramJeu
					.getTourActuel()) {

				// Ce de sera surligne en orange
				tabPanneauDe[nbDes].setBackground(SELECT);

			} else {

				// Le reste possede aucune couleur
				tabPanneauDe[nbDes].setBackground(null);

			} // Fin du if/else
			
		} // Fin du for

	}

	/************************************************************
	 * Ecouteur de JButton
	 ************************************************************/
	@Override
	public void actionPerformed(ActionEvent action) {

		if (action.getSource() == btnAction) {

			// Brasse les des
			paramJeu.brassage();

			// Lorsque la partie est terminee
			if (paramJeu.isTerminer()) {

				// Le bouton de brassage est desactive
				btnAction.setEnabled(false);

				// Dans le cas ou nous avons deux gagnants
				if (paramJeu.getJoueurGagnant().length > 1) {

					// Declaration
					String egalite = "";

					// On enumere tous les gagnants
					for (int i = 0; paramJeu.getJoueurGagnant().length > i; i++) {

						egalite = egalite + paramJeu.getJoueurGagnant()[i].NOM
								+ " ";

					}

					// On definit notre message
					vainqueur = "�galit� entre " + "\n " + egalite + " ( "
							+ paramJeu.getJoueurGagnant()[0].getPointage()
							+ " points ) \n";

				} else {

					// Pour un seul gagnant
					vainqueur = "Le vainqueur de cette partie est " + "\n "
							+ paramJeu.getJoueurGagnant()[0].NOM + " ( "
							+ paramJeu.getJoueurGagnant()[0].getPointage()
							+ " points ) \n";
				}

				// Affichage des gagnants
				JOptionPane.showMessageDialog(null, vainqueur, TITRE_GAGNANT,
						JOptionPane.INFORMATION_MESSAGE);

			}

			// Mise a jour de l'affichage
			rafraichir();

		}
		if (action.getSource() == btnRetour) {

			// Affichage d'un JOptionPane pour confirmation
			if (paramJeu.getTourActuel() > 1 && paramJeu.getTourActuel() < 5) {

				reponse = JOptionPane.showConfirmDialog(null, MESSAGE_QUITTER,
						TITRE_AVERTIR, JOptionPane.YES_NO_OPTION);

			} else {

				// Automatiquement 'oui'
				reponse = JOptionPane.YES_OPTION;

			}

			// Si l'utilisateur confirme
			if (reponse == JOptionPane.YES_OPTION) {

				// On ferme cette fenetre
				frame.dispose();

				// Retourne a la fenetre de parametre
				new AffichagePopup();

			}

		}
		if (action.getSource() == btnNouvellePartie) {

			// Affichage d'un JOptionPane pour confirmation
			if (paramJeu.getTourActuel() > 1 && paramJeu.getTourActuel() < 5) {

				reponse = JOptionPane.showConfirmDialog(null, MESSAGE_REC,
						TITRE_AVERTIR, JOptionPane.YES_NO_OPTION);

			} else {

				// Automatiquement 'oui'
				reponse = JOptionPane.YES_OPTION;

			}

			// Si l'utilisateur confirme ou le nombre de tour a atteint 6
			if (reponse == JOptionPane.YES_OPTION) {

				// Reinitialise tous les points/tours a zero
				paramJeu.recommencer();

				// Le bouton de brassage revient en marche
				btnAction.setEnabled(true);

			}

			// Mise a jour de l'affichage
			rafraichir();

		}

	}

}
