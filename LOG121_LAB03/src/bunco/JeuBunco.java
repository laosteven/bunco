/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		JeuBunco.java
 * Date cree: 			2013-07-01
 * Date dern. modif.: 	2013-07-01
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-07-01 Version initiale
 ***************************************************************/
package bunco;

import cadriciel.*;

/***************************************************************
 * Classe qui gere le deroulement du jeu et provient l'information 
 * necessaire a la classe AffichageJeu pour afficher:
 * 
 * Le pointage La position actuelle du joueur Les chiffres de des
 ***************************************************************/
public class JeuBunco extends IStrategieBunco {

	/***************************************************************
	 * Constantes
	 ***************************************************************/
	public final int NB_TOUR_MAX = 6;

	private final String GAGNE = "Joueur1 Bravo! + ";
	private final String BUNCO = "Joueur1 Bunco!!";
	private final String NEUTRE = "Joueur1 --";
	public final int NB_BRASSE_MAX = 3;
	
	/***************************************************************
	 * Attributs
	 ***************************************************************/
	private int nbTours;
	private int nbBrasse;
	private int numJoueurActuel;
	private int position;
	private int[] tabPointage;
	private Joueur[] posJoueur;
	
	private Joueur premierJoueur;
	private Joueur joueurActuel;
	
	private AffichageJeu affiche;
	
	CollectionJoueur listeJoueurs;
	CollectionDe listeDe;
	IStrategie strategieJeu;
	
	/***************************************************************
	 * Accesseurs
	 ***************************************************************/
	
	/***************************************************************
	 * @return Le nombre de tour passe
	 ***************************************************************/
	public int getTourActuel(){
		return nbTours;
	}
	
	/***************************************************************
	 * @return Le nom du gagnant de la partie
	 ***************************************************************/
	public Joueur[] getJoueurGagnant(){
		return strategieJeu.
				calculerLeVainqueur(listeJoueurs.getTabJoueur());
	}
	
	/************************************************************
	 * Obtenir le joueur qui joue actuellement
	 * 
	 * @return Joueur
	 ************************************************************/
	public Joueur getJoueurActuel() {
		return joueurActuel;
	}

	/************************************************************
	 * Obtenir le numero de joueur
	 * 
	 * @return Le numero de joueur qui joue actuellement
	 ************************************************************/
	public int getNumJoueurActuel() {
		return numJoueurActuel;
	}
	
	/************************************************************
	 * Obtenir la position du joueur 1
	 * 
	 * @return Le numero de position
	 ************************************************************/
	public int getPosition(){
		
		// On recueille un tableau de joueur classe en ordre
		posJoueur = strategieJeu.calculerPosition(listeJoueurs.getTabJoueur());
		
		// Ainsi que le premier joueur par defaut
		premierJoueur = listeJoueurs.getTabJoueur()[0];

		// On fait une recherche
		for(int boucle = 0; boucle < listeJoueurs.getTabJoueur().length; boucle ++){
		
			// En comparant les pointages afin de determiner la position
			if(posJoueur[boucle].getPointage() == premierJoueur.getPointage()){

				// La position du joueur
				position = boucle + 1;
				
			} // Fin du if
			
		} // Fin du for
				
		return position;
		
	}
	
	/************************************************************
	 * Creer un tableau de pointage pour l'affichage
	 * 
	 * @return Un tableau de pointage de chaque joueur
	 ************************************************************/
	public int[] getTabPointage(){

		// On associe le pointage de la liste pour chaque joueur
		for(int boucle = 0; 
				boucle < listeJoueurs.getTabJoueur().length; 
				boucle ++){

				tabPointage[boucle] = listeJoueurs.getTabJoueur()[boucle].getPointage();
			
		} // Fin du for

		return tabPointage;
		
	}
	
	/***************************************************************
	 * Constructeur de la classe JeuBunco.
	 * 
	 * @param listeDe
	 *            La collection de des crees
	 * @param listeJoueur
	 *            La collection de joueur qui participent
	 ***************************************************************/
	public JeuBunco(CollectionDe listeDe, CollectionJoueur listeJoueur) {
		
		// Creation d'une nouvelle strategie de jeu
		strategieJeu = new IStrategieBunco();
		
		// Les collections sont prises en reference pour le jeu Bunco
		this.listeDe = listeDe;
		this.listeJoueurs = listeJoueur;
		
		// Creation du tableau de pointage
		this.tabPointage = new int[listeJoueurs.getTabJoueur().length];
		
		// Initialisation de la partie
		recommencer();
		
		// On affiche le jeu en lui renvoyant les methodes de la classe
		affiche = new AffichageJeu(this);

	}


	/***************************************************************
	 * Determine si la partie est terminee ou non
	 * 
	 * @return Retourne lorsque le nombre de tours depasse 6
	 ***************************************************************/
	public boolean isTerminer() {
		return nbTours > NB_TOUR_MAX;
	}

	/***************************************************************
	 * Incrementation du nombre de tour pour la progression du jeu
	 ***************************************************************/
	public void addTour() {

		// Si on n'a pas atteint 6 tours
		if (!isTerminer())
			nbTours++;
		
	}

	/************************************************************
	 * G�re la rotation des joueurs. Ajoute un tour lorsque la 
	 * liste est �puis�.
	 ************************************************************/
	private void rotationJoueurs() {
		
		// Lorsque la liste s'epuise
		if (this.numJoueurActuel > listeJoueurs.getTabJoueur().length) {
			
			// On revient au premier joueur
			this.numJoueurActuel = 1;
			
			// Un tour vient de passer
			addTour();
			
		}

		// On passe au prochain joueur
		this.joueurActuel = listeJoueurs.getTabJoueur()[getNumJoueurActuel() - 1];
		numJoueurActuel ++;	
		
	}

	/***************************************************************
	 * On remet tout le pointage a zero et le nombre de tour 
	 * recommence a zero.
	 ***************************************************************/
	public void recommencer() {
		
		// Initialisation des variables
		this.nbTours = 1;
		this.nbBrasse = 1;
		this.numJoueurActuel = 1;
		
		// On repart pour le premier joueur
		rotationJoueurs();

		// On ajuste le pointage des joueurs a zero
		for(int boucle = 0; boucle < this.listeJoueurs.getTabJoueur().length; boucle ++)
			this.listeJoueurs.getTabJoueur()[boucle].initPointage();

	}

	/************************************************************
	 * G�re le brassage et la fin d'un tour de brassage.
	 ************************************************************/
	public void brassage() {
		
		// Le pointage est calcule
		int pointageBrassage = calculPointageBrasse();
		
		// On rajoute le pointage au joueur actuel
		this.joueurActuel.addPointage(pointageBrassage);
		
		// On recupere le pointage gagne par le joueur
		int pointTourActuel = getJoueurActuel().getDernierPointage();
		
		// On rajoute plus d'information pour le joueur 1
		if(getJoueurActuel().NOM.equals("Joueur1")){

			// Affichage des points cumules
			affiche.setLblPointage(getJoueurActuel().getPointage());
			
			// Affichage des points gagnes par tour
			affiche.setLblSomme(pointTourActuel);
			
			// Affichage lorsque le joueur gagne
			if(pointTourActuel == 0)
				affiche.setLblMessage(NEUTRE);
			else if(pointTourActuel > 0)
				affiche.setLblMessage(GAGNE + pointTourActuel);
			else if(pointTourActuel == 21)
				affiche.setLblMessage(BUNCO);
				
		} else {
			
			// Pour les autres joueurs
			
			// Affiche a qui le tour de jouer et leur point gagne
			affiche.setLblMessage(getJoueurActuel().NOM +
					" + " + pointTourActuel);
			
		}

		// On incremente le nombre de brassage permit
		nbBrasse++;
		
		// Si le joueur brasse plus de 3 fois ou:
		// - Obtient un bunco
		// - Aucun chiffre de des sont identiques
		if (strategieJeu.finBrasse(pointageBrassage) || nbBrasse > NB_BRASSE_MAX) {
			
			// Initialisation du nombre de brassage
			nbBrasse = 1;
			// On passe au prochain joueur
			rotationJoueurs();

		}

	}

	/************************************************************
	 * Brasse et calcul du pointage.
	 * 
	 * @return Valeur de pointage du dernier brassage.
	 ************************************************************/
	private int calculPointageBrasse() {
		
		// On reprend la collection de des
		int[] tabBrassageDe = new int[this.listeDe.getTabDe().length];

		// Pour tous les des
		for (int boucle = 0; boucle < this.listeDe.getTabDe().length; boucle++) {
			
			// On brasse
			this.listeDe.getTabDe()[boucle].brasserDe();
			
			// Et on enregistre dans le nouveau tableau de des
			tabBrassageDe[boucle] = this.listeDe.getTabDe()[boucle].getChiffre();
			
			// On affiche le nouveau jeu de des
			affiche.setDe(boucle, tabBrassageDe[boucle]);
			
		}
		
		return strategieJeu.calculerScoreTour(tabBrassageDe, nbTours);
		
	}

}
