/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		CollectionJoueur.java
 * Date cree: 			2013-06-27
 * Date dern. modif.: 	2013-06-30
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-06-27 Version initiale
 ***************************************************************/
package cadriciel;

import java.util.ArrayList;
import java.util.ListIterator;

/***************************************************************
 * G�re la liste des joueurs.
 ***************************************************************/
public class CollectionJoueur {
	
	/***************************************************************
	 * Attributs
	 ***************************************************************/
	ArrayList<Joueur> liste = new ArrayList<Joueur>();
	ListIterator<Joueur> listeIterator;
	private int numero;

	/***************************************************************
	 * Constructeur par defaut de la classe/
	 * Par defaut, le joueur 1 est joue par l'utilisateur
	 * 
	 * @param nbJoueurRef Le nombre de joueur qu'il faut creer
	 ***************************************************************/
	public CollectionJoueur(int nbJoueurRef) {
		
		for(numero = 1; numero < nbJoueurRef + 1; numero ++){
			
			// On definit le nom du joueur selon l'ordre qu'il
			// a ete cree
			ajouterJoueur(new Joueur("Joueur" + String.valueOf(numero)));
			
		}
		
	}
	
/***************************************************************
 * Ajoute un nouveau joueur dans la liste
 * @param Un nouveau Joueur
 ***************************************************************/
	private void ajouterJoueur(Joueur nouveauJoueur) {
		liste.add(nouveauJoueur);
		listeIterator = liste.listIterator();
	}
	
/***************************************************************
 * @return Le joueur suivant
 ***************************************************************/
	private Joueur getJoueurSuivant() {
		return listeIterator.next();

	}
/***************************************************************
 * @return S'il existe un joueur apr�s
 ***************************************************************/
	private boolean resteJoueurSuivant() {
		return listeIterator.hasNext();
	}
	
/***************************************************************
 * @return S'il existe un joueur avant
 ***************************************************************/
	private boolean resteJoueurPrecedant() {
		return listeIterator.hasPrevious();
	}
	
/***************************************************************
 * @return Le nombre d'�l�ments dans la liste
 ***************************************************************/
	public int getNbElements() {
		debutListeIterator();
		int nbElements = 0;
		while (resteJoueurSuivant()) {
			listeIterator.next();
			nbElements++;
		}
		return nbElements;
	}
	
/***************************************************************
 * @return Un tableau d'objet de joueur.
 ***************************************************************/
	public Joueur[] getTabJoueur() {
		Joueur[] tabJoueur = new Joueur[getNbElements()];
		int boucle = 0;
		debutListeIterator();
		while (resteJoueurSuivant()) {
			tabJoueur[boucle] = listeIterator.next();
			boucle++;
		}
		return tabJoueur;
	}
	
/***************************************************************
 * Initialise la liste d'it�rateur.
 ***************************************************************/
	private void debutListeIterator() {
		listeIterator = liste.listIterator();
	}
}
