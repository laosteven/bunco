/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		IStrategie.java
 * Date cree: 			2013-06-27
 * Date dern. modif.: 	
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-07-01 Version initiale
 ***************************************************************/
package cadriciel;

/***************************************************************
 * Interface qui permet l'affichage des details importants
 * lors d'une partie de jeu
 ***************************************************************/
public interface IStrategie {

	// Renvoie le joueur qui a gagne la partie
	public Joueur[] calculerLeVainqueur(Joueur tabjoueur[]);
	
	// Renvoie le tableau de joueur en ordre
	public Joueur[] calculerPosition(Joueur tabJoueur[]);

	// Calcule le pointage
	public int calculerScoreTour(int[] pointageDe, int tour);

	// Donne la permission au joueur de brasser
	public boolean finBrasse(int pointageBrassage);

}
