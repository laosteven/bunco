/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		Fabrique.java
 * Date cree: 			2013-06-27
 * Date dern. modif.: 	2013-06-30
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-06-27 Version initiale
 * 2013-06-30 Ajout des accesseurs/mutateurs. Integration avec
 * 			  JeuAffiche pour le moment.
 ***************************************************************/
package cadriciel;

import bunco.JeuBunco;

public class Fabrique {

	/****************************************************************
	 * Attributs
	 ****************************************************************/
	private int nbDes;
	private int nbJoueur;
	
	private CollectionDe jeuDe;
	private CollectionJoueur jeuJoueur;

	/****************************************************************
	 * Accesseurs
	 ****************************************************************/
	
	/****************************************************************
	 * @return Retourne le nombre de des recu des parametres
	 ****************************************************************/
	public int getNbDes() {
		return nbDes;
	}

	/****************************************************************
	 * @return Retourne le nombre de joueur recu des parametres
	 ****************************************************************/
	public int getNbJoueur() {
		return nbJoueur;
	}

	// TODO Je crois pas qu'on se sert des mutateurs... On les laisse
	// pour le moment
	
	
	/****************************************************************
	 * Mutateurs
	 ****************************************************************/
	
	/****************************************************************
	 * Defini le nombre de des pour la partie
	 * @param nbDes Nombre de des a creer
	 ****************************************************************/
	public void setNbDes(int nbDes) {
		this.nbDes = nbDes;
	}

	/****************************************************************
	 * Defini le nombre de joueurs pour la partie
	 * @param nbJoueur Nombre de joueur a creer
	 ****************************************************************/
	public void setNbJoueur(int nbJoueur) {
		this.nbJoueur = nbJoueur;
	}
	
	/****************************************************************
	 * Constructeur par defaut
	 * 
	 * @param paramRef Parametres du jeu prise en reference
	 ****************************************************************/
	public Fabrique(int nbJoueurRef, int nbDesRef,  int nbFaces) {

		// On ajuste les parametres selon le choix de l'utilisateur
		setNbJoueur(nbJoueurRef);
		setNbDes(nbDesRef);
		
		// Creation d'une collection de des
		jeuDe = new CollectionDe(nbDesRef, nbFaces);
		
		// Creation d'une collection de joueurs
		jeuJoueur = new CollectionJoueur(nbJoueurRef);
		
		// On affiche la fenetre de jeu
		new JeuBunco(jeuDe,jeuJoueur);
		
	}

}
