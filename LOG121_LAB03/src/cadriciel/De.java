/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		De.java
 * Date cree: 			2013-06-27
 * Date dern. modif.: 	2013-06-30
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-06-27 Version initiale
 ***************************************************************/
package cadriciel;

/***************************************************************
 * Contient les informations d'un d� cr��.
 * impl�mentation de l'interface Comparable<De> 
 ***************************************************************/
public class De implements Comparable<De>{
	
	/***************************************************************
	 * Attributs
	 ***************************************************************/
	public final int NBFACES;
	private int chiffre; 
	
	/***************************************************************
	 * Donne le chiffre obtenu sur la face du de
	 * @return Le chiffre sur la face du de
	 ***************************************************************/
	public int getChiffre(){
		return chiffre;
	}

/**
 * Constructeur de la classe D�.
 * @param nbFaces int le nombre de faces.
 */
	public De(int nbFaces) {
		
		// Initialisation du nombre de faces
		this.NBFACES = nbFaces;
		// Initialisation du chiffre de des
		brasserDe();
		
	}
	
	// Source:
	// http://stackoverflow.com/questions/363681/
	// 		generating-random-number-in-a-range-with-java
	/***************************************************************
	 * M�thode afin de g�n�r� un nombre al�atoire selon le D�.
	 ***************************************************************/
	public void brasserDe() {
		
		// Attributs
		int min = 1;
		int max = this.NBFACES;
		
		// Un chiffre est genere au hasard
		chiffre = min + (int)(Math.random() * (max - min + 1));

	}

	@Override
	public int compareTo(De arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}
