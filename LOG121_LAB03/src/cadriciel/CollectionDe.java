/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		CollectionDe.java
 * Date cree: 			2013-06-27
 * Date dern. modif.: 	2013-06-30
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-06-27 Version initiale
 ***************************************************************/
package cadriciel;

import java.util.*;

// exemple : http://www.tutorialspoint.com/java/java_using_iterator.htm

/***************************************************************
 * Classe que g�re la liste des D�s
 ***************************************************************/
public class CollectionDe {
	
	/***************************************************************
	 * Attributs
	 ***************************************************************/
	ArrayList<De> liste = new ArrayList<De>();
	ListIterator<De> listeIterator;

	/***************************************************************
	 * Constructeur par defaut: Construit un nombre specifique de des
	 * et de faces.
	 * 
	 * @param nbDesRef 	Nombre de des en reference
	 * @param nbFaces	Nombre de faces de de en reference
	 ***************************************************************/
	public CollectionDe(int nbDesRef, int nbFaces) {

		// Ajout de nouveaux des dans la liste
		for(int i=0; i < nbDesRef ; i++){
			ajouterDe(new De(nbFaces));
		}
		
	}

	/***************************************************************
	 * Ajouter des nouveau d�s
	 * 
	 * @param nouveauDe Nouveau de recu a ajouter dans la liste
	 ***************************************************************/
	private void ajouterDe(De nouveauDe) {
		
		liste.add(nouveauDe);
		listeIterator = liste.listIterator();
		
	}

	/***************************************************************
	 * @return Le d� suivant dans la liste
	 ***************************************************************/
	public De getDeSuivant() {
		return this.listeIterator.next();

	}

	/***************************************************************
	 * @return S'il reste un �l�ment apr�s dans la liste
	 ***************************************************************/
	private boolean resteDeSuivant() {
		return this.listeIterator.hasNext();
	}

//	/**
//	 * 
//	 * @return S'il reste un �l�ment avant dans la liste
//	 */
//	private boolean resteDePrecedant() {
//		return this.listeIterator.hasPrevious();
//	}

	/***************************************************************
	 * @return Le nombre d'�l�ment dans la liste
	 ***************************************************************/
	public int getNbElements() {
		debutListeIterator();
		int nbElements = 0;
		while (resteDeSuivant()) {
			this.listeIterator.next();
			nbElements++;
		}
		return nbElements;
	}

	/***************************************************************
	 * @return Le tableau de D�.
	 ***************************************************************/
	public De[] getTabDe() {
		De[] tabDe = new De[getNbElements()];
		int boucle = 0;

		debutListeIterator();
		while (resteDeSuivant()) {
			tabDe[boucle] = this.listeIterator.next();
			boucle++;
		}
		return tabDe;
	}

	/***************************************************************
	 * Initilialise la liste d'it�rateur.
	 ***************************************************************/
	private void debutListeIterator() {
		this.listeIterator = this.liste.listIterator();
	}
}
