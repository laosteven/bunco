/***************************************************************
 * Cours: 				LOG121 
 * Session: 			E2013
 * Groupe: 				01
 * Projet: 				Laboratoire #3
 * Etudiants:  			Anass Maai
 * 						Frederic Marquis
 * 						Steven Lao 
 * Professeur: 			Patrice Boucher
 * Nom du fichier: 		Joueur.java
 * Date cree: 			2013-06-27
 * Date dern. modif.: 	2013-06-30
 *************************************************************** 
 * Historique des modifications
 *************************************************************** 
 * 2013-06-27 Version initiale
 ***************************************************************/
package cadriciel;

/***************************************************************
 * Contient les informations d'un joueur.
 * impl�mentation de l'interface Comparable<Joueur> 
 ***************************************************************/
public class Joueur implements Comparable<Joueur>{

	/***************************************************************
	 * Attributs
	 ***************************************************************/
	public final String NOM;   
	private int pointage;				
	private int dernierPointage;

	/***************************************************************
	 * Contructeur de la classe Joueur. Besoin d'un nom de joueur en param�tre.
	 * 
	 * @param nomJoueur
	 *            L'identification du joueur
	 ***************************************************************/
	public Joueur(String nomJoueur) {
		this.NOM = nomJoueur;
		initPointage();
	}
	
	/***************************************************************
	 * Initialise le pointage du joueur a zero.
	 ***************************************************************/
	public void initPointage(){
		pointage = 0;
		dernierPointage = 0;
	}

	/***************************************************************
	 * @return Retourne le pointage du joueur
	 ***************************************************************/
	public int getPointage() {
		return this.pointage;
	}

	/***************************************************************
	 * M�thode pour ajouter le pointage additionnel au joueur
	 * 
	 * @param ajoutPointage
	 *            Le pointage � ajouter au joueur.
	 ***************************************************************/
	public void addPointage(int ajoutPointage) {
		this.dernierPointage = ajoutPointage;
		this.pointage = this.pointage + ajoutPointage;
	}

	/***************************************************************
	 * Accesseur du dernier pointage du joueur
	 * 
	 * @return Son dernier pointage
	 ***************************************************************/
	public int getDernierPointage() {
		return this.dernierPointage;
	}

	/***************************************************************
	 * Compare le pointage entre joueurs
	 ***************************************************************/
	@Override
	public int compareTo(Joueur compareJoueur) {
		
		// Attribut a comparer
		int comparePointage = compareJoueur.getPointage();
		 
		// Decroissant
		return  comparePointage - this.pointage;
		
	}
	
}
