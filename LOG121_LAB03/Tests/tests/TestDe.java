/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import cadriciel.De;

/**
 * @author Tech Fred
 *
 */
public class TestDe {
//	int[] tabde = {1,2,3,4,5,6};
	De testDe = new De(6);

	@Test
	public void testDe() {
		assertEquals(6 , testDe.NBFACES);
//		assertEquals(tabde , TestDe.TABCHIFFRES);
		
// http://stackoverflow.com/questions/14811014/writing-a-junit-test-for-a-random-number-generator
	}
	@Test
	public void testRandom() {
	    int pins;
	    int lowestPin = 10000;
	    int highestPin = -10000;

	    for (int i = 0; i < 10000000; i++) {
	    	testDe.brasserDe();
	        pins = testDe.getChiffre();
	        if (pins > testDe.NBFACES) {
	            fail("Nombre maximum est de " + pins);
	        }
	        if (pins < 1) {
	            fail("Le nombre minimum est de " + pins);
	        }

	        if (highestPin < pins) {
	            highestPin = pins;
	        }

	        if (lowestPin > pins) {
	            lowestPin = pins;
	        }
	    }

	    if (lowestPin == highestPin) {
	        fail("Un seul nombre est retourn� : " + highestPin);
	    }

	    if (lowestPin != 1) {
	        fail("La valeur la plus basse est de " + lowestPin);
	    }

	    if (highestPin != testDe.NBFACES) {
	        fail("La valeur la plus haute est de : "+ highestPin);
	    }

	}
}
