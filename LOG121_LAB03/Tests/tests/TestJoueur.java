package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import cadriciel.Joueur;

public class TestJoueur {
	Joueur joueurtest = new Joueur("Joueur1");

	@Test
	public void creerJoueur() {
		String actual = joueurtest.NOM;
		String expected = "Joueur1";
		assertEquals(expected, actual);
	}
	@Test
	public void pointage() {
		int expected = 12;
		joueurtest.addPointage(expected);
		int actual = joueurtest.getPointage();
		assertEquals(expected, actual);


	}
	@Test
	public void ajouterPoints(){
		
		joueurtest.addPointage(10);
		joueurtest.addPointage(12);
		joueurtest.addPointage(5);
		int actual = joueurtest.getPointage();
		int expected = 27;
		assertEquals(expected, actual);
	}

}
