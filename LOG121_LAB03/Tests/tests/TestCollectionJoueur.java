package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import cadriciel.CollectionJoueur;

public class TestCollectionJoueur {
	CollectionJoueur testObj = new CollectionJoueur(3);



	@Test
	public void testGetNbElements() {
		testObj = new CollectionJoueur(3);
		assertEquals(3, testObj.getNbElements()); 
		testObj = new CollectionJoueur(5);
		assertEquals(5, testObj.getNbElements()); 
	}

	@Test
	public void testGetTabJoueur() {
		testObj = new CollectionJoueur(3);
		testObj.getTabJoueur()[0].addPointage(5);
		testObj.getTabJoueur()[1].addPointage(6);
		testObj.getTabJoueur()[2].addPointage(7);
		
		assertEquals(5, testObj.getTabJoueur()[0].getPointage());
		assertEquals(6, testObj.getTabJoueur()[1].getPointage());
		assertEquals(7, testObj.getTabJoueur()[2].getPointage());
	}

}
