package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import bunco.IStrategieBunco;
import cadriciel.CollectionJoueur;
import cadriciel.IStrategie;
import cadriciel.Joueur;

public class TestIstrategieBunco {
	IStrategie testObj = new IStrategieBunco();

	@Test
	public void testCalculerScoreTour() {

		int[] points5 = { 4, 3, 1 };
		assertEquals(1, testObj.calculerScoreTour(points5, 1));

		int[] points = { 2, 2, 2 };
		assertEquals(21, testObj.calculerScoreTour(points, 2));

		int[] points1 = { 2, 3, 2 };
		assertEquals(2, testObj.calculerScoreTour(points1, 2));

		int[] points2 = { 1, 5, 7 };
		assertEquals(0, testObj.calculerScoreTour(points2, 2));

		int[] points3 = { 2, 2, 2 };
		assertEquals(5, testObj.calculerScoreTour(points3, 5));

		int[] points4 = { 5, 2, 2 };
		assertEquals(0, testObj.calculerScoreTour(points4, 1));

	}

	@Test
	public void testFinBrasse() {
		assertEquals(true, testObj.finBrasse(0));
		assertEquals(true, testObj.finBrasse(21));
		assertEquals(false, testObj.finBrasse(3));

	}

	@Test
	public void testCalculerLeVainqueur() {

		CollectionJoueur testColJoueurs = new CollectionJoueur(5);

		testColJoueurs.getTabJoueur()[0].addPointage(4);
		testColJoueurs.getTabJoueur()[1].addPointage(3);
		testColJoueurs.getTabJoueur()[2].addPointage(11);
		testColJoueurs.getTabJoueur()[3].addPointage(9);
		testColJoueurs.getTabJoueur()[4].addPointage(7);

		Joueur[] actuals = testObj.calculerLeVainqueur(testColJoueurs
				.getTabJoueur());
		assertArrayEquals(testColJoueurs.getTabJoueur()[2], actuals[0]);

		testColJoueurs.getTabJoueur()[4].addPointage(10);
		assertArrayEquals(testColJoueurs.getTabJoueur()[2], actuals[0]);

	}

	@Test
	public void testCalculerEgalite() {

		CollectionJoueur testColJoueurs = new CollectionJoueur(5);

		testColJoueurs.getTabJoueur()[0].addPointage(3);
		testColJoueurs.getTabJoueur()[1].addPointage(3);
		testColJoueurs.getTabJoueur()[2].addPointage(11);
		testColJoueurs.getTabJoueur()[3].addPointage(11);
		testColJoueurs.getTabJoueur()[4].addPointage(1);

		Joueur[] actuals = testObj.calculerLeVainqueur(testColJoueurs
				.getTabJoueur());

		if ((testColJoueurs.getTabJoueur()[2] == actuals[0])
				|| (testColJoueurs.getTabJoueur()[3] == actuals[0])) {

		} else {
			fail("V�rifier l'�galit�");
		}
		if (((testColJoueurs.getTabJoueur()[2] == actuals[1]) || (testColJoueurs
				.getTabJoueur()[3] == actuals[1]))) {
		} else {
			fail("V�rifier l'�galit�");
		}

		testColJoueurs.getTabJoueur()[4].addPointage(10);
		assertArrayEquals(testColJoueurs.getTabJoueur()[2], actuals[0]);

	}

	private void assertArrayEquals(Joueur joueur, Joueur actuals) {
		// TODO Auto-generated method stub

	}

	@Test
	public void testJouerTour() {
		fail("Not yet implemented");
	}

}
