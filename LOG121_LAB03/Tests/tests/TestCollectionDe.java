package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cadriciel.CollectionDe;

public class TestCollectionDe {
	CollectionDe listeDe = new CollectionDe(3, 6);

	// int[] tabde1 = {1,2,3,4,5,6};
	// int[] tabde2 = {1,2,3,5,6};
	// int[] tabde3 = {1,3,4,5,6};
	//
	// De de1 = new De();
	// De de2 = new De();
	// De de3 = new De();
	// De De4 = new De(tabde3);

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testGetDeSuivant() {
		assertEquals(6, listeDe.getDeSuivant().NBFACES);
		assertEquals(6, listeDe.getDeSuivant().NBFACES);
		assertEquals(6, listeDe.getDeSuivant().NBFACES);

	}

	@Test
	public void testGetNbElements() {
		assertEquals(3, listeDe.getNbElements());
	}

	@Test
	public void testGetNbElements2() {
		CollectionDe listeDe2 = new CollectionDe(4, 6);
		assertEquals(4, listeDe2.getNbElements());
	}

	@Test
	public void testGetTabDe() {
		assertEquals(6, listeDe.getTabDe()[0].NBFACES);
		assertEquals(6, listeDe.getTabDe()[1].NBFACES);
		assertEquals(6, listeDe.getTabDe()[2].NBFACES);

	}

}
