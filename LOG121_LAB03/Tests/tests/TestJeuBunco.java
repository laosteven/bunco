package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import cadriciel.CollectionDe;
import cadriciel.CollectionJoueur;

import bunco.JeuBunco;

public class TestJeuBunco {
	CollectionDe testDe = new CollectionDe(3, 6);
	CollectionJoueur testJoueurs = new CollectionJoueur(4);
	JeuBunco testJeu = new JeuBunco(testDe, testJoueurs);

	@Test
	public void testRecommencer() {
		fail("Not yet implemented");
	}

	@Test
	public void testBrassageSuivant() {
		int highestPin = -50000;
		int lowestPin = 50000;
		for (int i = 0; i < 200; i++) {
			testJeu = new JeuBunco(testDe, testJoueurs);

			assertEquals(testJoueurs.getTabJoueur()[0], testJeu.getJoueurActuel());
			testJeu.brassage();

			if ((testJoueurs.getTabJoueur()[0].getDernierPointage() == 21) || testJoueurs.getTabJoueur()[0].getDernierPointage() == 0) {
				assertNotSame(testJoueurs.getTabJoueur()[0], testJeu.getJoueurActuel());
				testJeu = new JeuBunco(testDe, testJoueurs);
			} else {
				int j = 1;
				while (!((testJoueurs.getTabJoueur()[0].getDernierPointage() == 21) || (testJoueurs.getTabJoueur()[0].getDernierPointage() == 0)) && (testJeu.NB_BRASSE_MAX > j)) {

					assertEquals(testJoueurs.getTabJoueur()[0], testJeu.getJoueurActuel());
					if (lowestPin > j) {
						lowestPin = j;
					}

					testJeu.brassage();
					j++;

					if (highestPin < j) {
						highestPin = j;
					}


				}

			}
		}
		if (lowestPin == highestPin) {
			fail("Un seul nombre est retourn� : " + highestPin);
		}

		if (lowestPin != 1) {
			fail("La valeur la plus basse est de " + lowestPin);
		}

		if (highestPin != testJeu.NB_BRASSE_MAX) {
			fail("La valeur la plus haute est de : " + highestPin);
		}
	}

	@Test
	public void testBrassageTours() {

		testJeu = new JeuBunco(testDe, testJoueurs);

	}

}
